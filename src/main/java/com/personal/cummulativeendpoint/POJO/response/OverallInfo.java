package com.personal.cummulativeendpoint.POJO.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class OverallInfo {


    private PersonalInfo personaldetails;
    private ExperienceInfo[] experiencedetails;
    private EducationInfo[] educationdetails;

    public PersonalInfo getPersonaldetails() {
        return personaldetails;
    }

    public void setPersonaldetails(PersonalInfo personaldetails) {
        this.personaldetails = personaldetails;
    }

    public ExperienceInfo[] getExperiencedetails() {
        return experiencedetails;
    }

    public void setExperiencedetails(ExperienceInfo[] experiencedetails) {
        this.experiencedetails = experiencedetails;
    }

    public EducationInfo[] getEducationdetails() {
        return educationdetails;
    }

    public void setEducationdetails(EducationInfo[] educationdetails) {
        this.educationdetails = educationdetails;
    }
}
