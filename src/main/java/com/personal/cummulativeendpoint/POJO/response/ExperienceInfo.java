package com.personal.cummulativeendpoint.POJO.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.Date;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ExperienceInfo {

    private int personID;
    private String ClientCompany;
    private String ContractingCompany;
    private String Team;
    private String Project;
    private Date StartDate;
    private Date EndDate;
    private String Roles;
    private String Responsibilities;
    private String TechStack;

    public int getPersonID() {
        return personID;
    }

    public void setPersonID(int personID) {
        this.personID = personID;
    }

    public String getClientCompany() {
        return ClientCompany;
    }

    public void setClientCompany(String clientCompany) {
        ClientCompany = clientCompany;
    }

    public String getContractingCompany() {
        return ContractingCompany;
    }

    public void setContractingCompany(String contractingCompany) {
        ContractingCompany = contractingCompany;
    }

    public String getTeam() {
        return Team;
    }

    public void setTeam(String team) {
        Team = team;
    }

    public String getProject() {
        return Project;
    }

    public void setProject(String project) {
        Project = project;
    }

    public Date getStartDate() {
        return StartDate;
    }

    public void setStartDate(Date startDate) {
        StartDate = startDate;
    }

    public Date getEndDate() {
        return EndDate;
    }

    public void setEndDate(Date endDate) {
        EndDate = endDate;
    }

    public String getRoles() {
        return Roles;
    }

    public void setRoles(String roles) {
        Roles = roles;
    }

    public String getResponsibilities() {
        return Responsibilities;
    }

    public void setResponsibilities(String responsibilities) {
        Responsibilities = responsibilities;
    }

    public String getTechStack() {
        return TechStack;
    }

    public void setTechStack(String techStack) {
        TechStack = techStack;
    }

}
