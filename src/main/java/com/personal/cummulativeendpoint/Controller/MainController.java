package com.personal.cummulativeendpoint.Controller;


import com.personal.cummulativeendpoint.Common.exceptions.customUncheckedException;
import com.personal.cummulativeendpoint.POJO.request.InputMaster;
import com.personal.cummulativeendpoint.POJO.response.EducationInfo;
import com.personal.cummulativeendpoint.POJO.response.ExperienceInfo;
import com.personal.cummulativeendpoint.POJO.response.OverallInfo;
import com.personal.cummulativeendpoint.POJO.response.PersonalInfo;
import com.personal.cummulativeendpoint.Service.Impl.cummulativeEndpointImpl;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.util.List;


@RestController
public class MainController
{
    Logger logger = LoggerFactory.getLogger(PersonalInfo.class);

    @Autowired
    cummulativeEndpointImpl cummulativeendpointServiceObj;

    @ApiOperation(value = "Overall Information fetch using the Input")
    @PostMapping(path = "/overallInformation")
    public @ResponseBody OverallInfo getOverallInfo(@RequestBody InputMaster inputMaster)
    {
        logger.info("Education Information Controller is being executed");
        List<EducationInfo> educationInfo;

        try {
               return  cummulativeendpointServiceObj.mixerServiceTemplate(inputMaster);

        } catch (customUncheckedException runtimeException) {
            logger.error("Error Message : " + runtimeException.getMessage());
            logger.error("Error Cause : " + runtimeException.getCause());
            throw new customUncheckedException(
                    "This unchecked exception is thrown in the Overall Controller.\n "
                            + runtimeException.getMessage(),
                    runtimeException);
        }
    }
}
