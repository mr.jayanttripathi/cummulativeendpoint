package com.personal.cummulativeendpoint.Service.Impl;



import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.personal.cummulativeendpoint.Common.exceptions.mixerrErrorHandler;
import com.personal.cummulativeendpoint.POJO.request.InputMaster;
import com.personal.cummulativeendpoint.POJO.response.EducationInfo;
import com.personal.cummulativeendpoint.POJO.response.ExperienceInfo;
import com.personal.cummulativeendpoint.POJO.response.OverallInfo;
import com.personal.cummulativeendpoint.POJO.response.PersonalInfo;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class cummulativeEndpointImpl  {

    @HystrixCommand(fallbackMethod = "handlefallbackService")
    public OverallInfo mixerServiceTemplate(InputMaster inputMaster) {


        OverallInfo overallInfo=new OverallInfo();

        RestTemplateBuilder restTemplateBuilder=new RestTemplateBuilder();
        RestTemplate restTemplate = restTemplateBuilder
                .errorHandler(new mixerrErrorHandler())
                .build();


        PersonalInfo personalInfo = restTemplate.postForObject("http://localhost:8080/PersonalInfo",inputMaster,PersonalInfo.class);
        ExperienceInfo[] experienceInfo = restTemplate.postForObject("http://localhost:8080/ExperienceInfo",inputMaster,ExperienceInfo[].class);
        EducationInfo[] educationInfo = restTemplate.postForObject("http://localhost:8080/EducationInfo",inputMaster,EducationInfo[].class);
        overallInfo.setEducationdetails(educationInfo);
        overallInfo.setExperiencedetails(experienceInfo);
        overallInfo.setPersonaldetails(personalInfo);

        return overallInfo;
    }

    @SuppressWarnings("unused")
    public OverallInfo handlefallbackService(InputMaster inputMaster)
    {
        OverallInfo overallInfo=new OverallInfo();
        PersonalInfo personalInfo=new PersonalInfo();
        personalInfo.setFirstName("Error");
        overallInfo.setPersonaldetails(personalInfo);
        return overallInfo;
    }

}
