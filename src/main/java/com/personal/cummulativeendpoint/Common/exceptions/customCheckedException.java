package com.personal.cummulativeendpoint.Common.exceptions;

public class customCheckedException extends Exception {



    public customCheckedException() {
        super();
    }

    public customCheckedException(String message) {
        super(message);
    }

    public customCheckedException(String message, Throwable cause) {
        super(message, cause);
    }

    public customCheckedException(Throwable cause) {
        super(cause);
    }

    protected customCheckedException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
