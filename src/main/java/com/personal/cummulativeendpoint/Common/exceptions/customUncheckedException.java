package com.personal.cummulativeendpoint.Common.exceptions;

public class customUncheckedException extends RuntimeException {


    public customUncheckedException() {
    }

    public customUncheckedException(String message) {
        super(message);
    }

    public customUncheckedException(String message, Throwable cause) {
        super(message, cause);
    }

    public customUncheckedException(Throwable cause) {
        super(cause);
    }

    public customUncheckedException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
